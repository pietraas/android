package com.example.piotr.myweather;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity implements IFragmentContainer{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main);
            replaceFragment(new ListFragment(), "list fragment");
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.activity_main);
            replaceFragmentLeft(new ListFragment(), "list fragment");
            replaceFragmentRight(new WeatherFragment(),"weather fragment");
        }
    }

    public void replaceFragment(Fragment fragment, String stackid) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, stackid);
        fragmentTransaction.addToBackStack(stackid);
        fragmentTransaction.commit();
    }
    public void replaceFragmentLeft(Fragment fragment, String stackid) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerLeft, fragment, stackid);
        fragmentTransaction.addToBackStack(stackid);
        fragmentTransaction.commit();
    }
    public void replaceFragmentRight(Fragment fragment, String stackid) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerRight, fragment, stackid);
        fragmentTransaction.addToBackStack(stackid);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed()
    {
        if(getFragmentManager().getBackStackEntryCount() > 1)
            getFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }
}
