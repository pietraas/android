package com.example.piotr.myweather;

import android.os.AsyncTask;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Pietras on 2015-05-10.
 */
public class GetHTTP extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {
        String returnJsonString = null;
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(params[0])
                .build();

        try {
            Response response = client.newCall(request).execute();
            returnJsonString = response.body().string();
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return returnJsonString;
    }
}