package com.example.piotr.myweather;

import android.app.Fragment;

/**
 * Created by Piotr on 2015-05-04.
 */
public interface IFragmentContainer {
    public void replaceFragment(Fragment fragment, String stackid);
    public void replaceFragmentLeft(Fragment fragment, String stackid);
    public void replaceFragmentRight(Fragment fragment, String stackid);
}
