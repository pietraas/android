package com.example.piotr.myweather;

/**
 * Created by Piotr on 2015-05-09.
 */
public class ItemList {

    private String itemName;

    public ItemList(String itemName) {
        super();
        this.itemName = itemName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

}
