package com.example.piotr.myweather;

import java.util.ArrayList;

/**
 * Created by Piotr on 2015-05-09.
 */
public class SubCategory {

    private String pSubCatName;
    private ArrayList<ItemList> mItemListArray;

    public SubCategory(String pSubCatName,
                       ArrayList<ItemList> mItemListArray) {
        super();
        this.pSubCatName = pSubCatName;
        this.mItemListArray = mItemListArray;
    }

    public String getpSubCatName() {
        return pSubCatName;
    }

    public void setpSubCatName(String pSubCatName) {
        this.pSubCatName = pSubCatName;
    }

    public ArrayList<ItemList> getmItemListArray() {
        return mItemListArray;
    }

    public void setmItemListArray(ArrayList<ItemList> mItemListArray) {
        this.mItemListArray = mItemListArray;
    }
}