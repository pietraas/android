package com.example.piotr.myweather;

/**
 * Created by Piotr on 2015-05-04.
 */
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class ListFragment extends Fragment {

    private ArrayList<Items>mainList;
    private ArrayList<SubCategory>subArrayList1;
    private ArrayList<SubCategory>subArrayList2;
    private LinearLayout mLinearListView;
    boolean isFirstViewClick=false;
    boolean isSecondViewClick=false;

    private IFragmentContainer fragmentContainer;


    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentContainer = (IFragmentContainer) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_list,container,false);
        mLinearListView = (LinearLayout) rootView.findViewById(R.id.linear_listview);

        //Make array list one is for mainlist and other is for sublist
        mainList=new ArrayList<Items>();
        subArrayList1=new ArrayList<SubCategory>();
        subArrayList2=new ArrayList<SubCategory>();

        //ArrayListy do sublist
        ArrayList<ItemList> miastaPolska=new ArrayList<ItemList>();
        ArrayList<ItemList> miastaHiszpania=new ArrayList<ItemList>();
        ArrayList<ItemList> miastaArgentyna=new ArrayList<ItemList>();
        ArrayList<ItemList> miastaBrazylia=new ArrayList<ItemList>();

        //Kontynenty
        mainList.add(new Items("Europa", subArrayList1));
        mainList.add(new Items("Ameryka Południowa", subArrayList2));

        //Kraje Europa
        subArrayList1.add(new SubCategory("Polska", miastaPolska));
        subArrayList1.add(new SubCategory("Hiszpania", miastaHiszpania));

        //Miasta Polska
        miastaPolska.add(new ItemList("Łódź"));
        miastaPolska.add(new ItemList("Wrocław"));

        //Miasta Hiszpania
        miastaHiszpania.add(new ItemList("Madryt"));
        miastaHiszpania.add(new ItemList("Barcelona"));

        //Kraje Ameryka Południowa
        subArrayList2.add(new SubCategory("Argentyna", miastaArgentyna));
        subArrayList2.add(new SubCategory("Brazylia", miastaBrazylia));

        //Miasta Argentyna
        miastaArgentyna.add(new ItemList("Córdoba"));
        miastaArgentyna.add(new ItemList("Rosario"));

        //Miasta Brazylia
        miastaBrazylia.add(new ItemList("Recife"));
        miastaBrazylia.add(new ItemList("Salvador"));





        //Adds data into first row
        for (int i = 0; i < mainList.size(); i++)
        {

            LayoutInflater inflater2 = null;
            inflater2 = (LayoutInflater) this.getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearView = inflater2.inflate(R.layout.row_first, null);

            final TextView mProductName = (TextView) mLinearView.findViewById(R.id.textViewName);
            final RelativeLayout mLinearFirstArrow=(RelativeLayout)mLinearView.findViewById(R.id.linearFirst);
            final ImageView mImageArrowFirst=(ImageView)mLinearView.findViewById(R.id.imageFirstArrow);
            final LinearLayout mLinearScrollSecond=(LinearLayout)mLinearView.findViewById(R.id.linear_scroll);

            //checkes if menu is already opened or not
            if(isFirstViewClick==false)
            {
                mLinearScrollSecond.setVisibility(View.GONE);
                mImageArrowFirst.setBackgroundResource(R.drawable.arw_lt);
            }
            else
            {
                mLinearScrollSecond.setVisibility(View.VISIBLE);
                mImageArrowFirst.setBackgroundResource(R.drawable.arw_down);
            }
            //Handles onclick effect on list item
            mLinearFirstArrow.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {

                    if(isFirstViewClick==false)
                    {
                        isFirstViewClick=true;
                        mImageArrowFirst.setBackgroundResource(R.drawable.arw_down);
                        mLinearScrollSecond.setVisibility(View.VISIBLE);

                    }
                    else
                    {
                        isFirstViewClick=false;
                        mImageArrowFirst.setBackgroundResource(R.drawable.arw_lt);
                        mLinearScrollSecond.setVisibility(View.GONE);
                    }
                    return false;
                }
            });


            final String name = mainList.get(i).getpName();
            mProductName.setText(name);

            //Adds data into second row
            for (int j = 0; j < mainList.get(i).getmSubCategoryList().size(); j++)
            {
                LayoutInflater inflater3 = null;
                inflater3 = (LayoutInflater) this.getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mLinearView2 = inflater3.inflate(R.layout.row_second, null);

                TextView mSubItemName = (TextView) mLinearView2.findViewById(R.id.textViewTitle);
                final RelativeLayout mLinearSecondArrow=(RelativeLayout)mLinearView2.findViewById(R.id.linearSecond);
                final ImageView mImageArrowSecond=(ImageView)mLinearView2.findViewById(R.id.imageSecondArrow);
                final LinearLayout mLinearScrollThird=(LinearLayout)mLinearView2.findViewById(R.id.linear_scroll_third);

                //checkes if menu is already opened or not
                if(isSecondViewClick==false)
                {
                    mLinearScrollThird.setVisibility(View.GONE);
                    mImageArrowSecond.setBackgroundResource(R.drawable.arw_lt);
                }
                else
                {
                    mLinearScrollThird.setVisibility(View.VISIBLE);
                    mImageArrowSecond.setBackgroundResource(R.drawable.arw_down);
                }
                //Handles onclick effect on list item
                mLinearSecondArrow.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {
                        if(isSecondViewClick==false)
                        {
                            isSecondViewClick=true;
                            mImageArrowSecond.setBackgroundResource(R.drawable.arw_down);
                            mLinearScrollThird.setVisibility(View.VISIBLE);

                        }
                        else
                        {
                            isSecondViewClick=false;
                            mImageArrowSecond.setBackgroundResource(R.drawable.arw_lt);
                            mLinearScrollThird.setVisibility(View.GONE);
                        }
                        return false;
                    }
                });


                final String catName = mainList.get(i).getmSubCategoryList().get(j).getpSubCatName();
                mSubItemName.setText(catName);
                //Adds items in subcategories
                for (int k = 0; k < mainList.get(i).getmSubCategoryList().get(j).getmItemListArray().size(); k++)
                {
                    LayoutInflater inflater4 = null;
                    inflater4 = (LayoutInflater) this.getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View mLinearView3 = inflater4.inflate(R.layout.row_third, null);
                    final TextView mItemName = (TextView) mLinearView3.findViewById(R.id.textViewItemName);
                    final String itemName = mainList.get(i).getmSubCategoryList().get(j).getmItemListArray().get(k).getItemName();
                    mItemName.setText(itemName);
                    mLinearScrollThird.addView(mLinearView3);
                    mItemName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(v instanceof TextView) {
                                String cityName;
                                cityName=(((TextView) v).getText().toString());
                                System.out.println("itemName: " + cityName);
                                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                                    fragmentContainer.replaceFragment(new WeatherFragment(cityName), "weatherFrag");
                                }else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                                {
                                    fragmentContainer.replaceFragmentRight(new WeatherFragment(), "weatherFrag1");
                                    fragmentContainer.replaceFragmentRight(new WeatherFragment(cityName), "weatherFrag");
                                }

                            }
                        }
                    });
                }

                mLinearScrollSecond.addView(mLinearView2);
            }
            mLinearListView.addView(mLinearView);
        }



        return rootView;

    }

}
